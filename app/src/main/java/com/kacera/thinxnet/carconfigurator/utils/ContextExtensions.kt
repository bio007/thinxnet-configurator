package com.kacera.thinxnet.carconfigurator.utils

import android.content.Context
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat

internal fun Context.getDrawableWithAnimation(resourceId: Int): Drawable? =
    ContextCompat.getDrawable(this, resourceId)?.also {
        if (it is Animatable) {
            it.start()
        }
    }