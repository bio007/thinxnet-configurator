package com.kacera.thinxnet.carconfigurator.ui.configuration.data

import com.kacera.thinxnet.carconfigurator.network.data.ApiResponse
import com.kacera.thinxnet.carconfigurator.network.data.Maker
import com.kacera.thinxnet.carconfigurator.network.data.Model

interface CarConfiguratorRepository {

    suspend fun getMakers(): ApiResponse<List<Maker>>
    suspend fun getModels(makerId: Long): ApiResponse<List<Model>>
}