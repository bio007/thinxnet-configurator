package com.kacera.thinxnet.carconfigurator.network

import com.kacera.thinxnet.carconfigurator.network.data.Makers
import com.kacera.thinxnet.carconfigurator.network.data.Models
import retrofit2.http.GET
import retrofit2.http.Path

interface ConfiguratorApi {

    companion object {
        const val BASE_URL = "https://tt4.thinxcloud-staging.de/vehicles/"
    }

    @GET("makes")
    suspend fun getMakers(): Makers

    @GET("makes/{makerId}/models")
    suspend fun getModels(@Path("makerId") makerId: Long): Models
}