package com.kacera.thinxnet.carconfigurator.persistance.data

import android.net.Uri
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CarConfiguration(
    val maker: String,
    val makerId: Long,
    val model: String,
    val modelId: Long,
    val fuelType: String,
    val fuelTypeId: Long,
    val url: Uri?
) : Parcelable {
    companion object {
        val EMPTY = CarConfiguration("", -1, "", -1, "", -1, null)
    }
}