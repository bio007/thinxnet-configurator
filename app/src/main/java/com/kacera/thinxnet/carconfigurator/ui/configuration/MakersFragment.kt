package com.kacera.thinxnet.carconfigurator.ui.configuration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.kacera.thinxnet.carconfigurator.databinding.LayoutFragmentMakersBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MakersFragment : Fragment() {

    private val viewModel by viewModel<MakersFragmentViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycle.addObserver(viewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return with(LayoutFragmentMakersBinding.inflate(inflater, container, false)) {
            lifecycleOwner = viewLifecycleOwner
            viewModel = this@MakersFragment.viewModel
                root
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.maker.observe(viewLifecycleOwner, Observer {
            it.navigate(parentFragmentManager)
        })
    }
}