package com.kacera.thinxnet.carconfigurator.utils

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.squareup.picasso.RequestCreator
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation

sealed class DynamicRequestCreator {

    abstract fun loadImage(target: ImageView)

    class PicassoRequestCreator(
        private val requestCreator: RequestCreator,
        @DrawableRes private val placeholderResId: Int = 0
    ) : DynamicRequestCreator() {
        override fun loadImage(target: ImageView) {
            if (placeholderResId > 0) {
                val placeholder = target.context.getDrawableWithAnimation(placeholderResId)
                if (placeholder != null) {
                    requestCreator.placeholder(placeholder) //Picasso can't handle vectors with theme attributes
                } else {
                    requestCreator.noPlaceholder()
                }
            } else {
                requestCreator.noPlaceholder()
            }
            requestCreator.transform(RoundedCornersTransformation(80, 10))
            requestCreator.into(target)
        }
    }

    class DrawableRequestCreator(@DrawableRes private val drawableRes: Int) :
        DynamicRequestCreator() {
        override fun loadImage(target: ImageView) {
            target.setImageDrawable(target.context.getDrawableWithAnimation(drawableRes))
        }
    }
}

