package com.kacera.thinxnet.carconfigurator.network

import com.kacera.thinxnet.carconfigurator.network.data.RemoteImage
import retrofit2.http.GET
import retrofit2.http.Query

interface UnsplashApi {

    companion object {
        const val BASE_URL = "https://api.unsplash.com/"
    }

    @GET("photos/random")
    suspend fun getImage(@Query(value = "query") query: String, @Query(value = "orientation") orientation: String? = null, @Query(value = "client_id") clientId: String = "uCxLBmUwhwp6gvK_mqDhzh3Sre0oq9iAOoMf4O68hKM"): RemoteImage
}