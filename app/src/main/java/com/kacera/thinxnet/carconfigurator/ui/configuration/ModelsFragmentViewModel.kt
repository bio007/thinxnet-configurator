package com.kacera.thinxnet.carconfigurator.ui.configuration

import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.kacera.thinxnet.carconfigurator.R
import com.kacera.thinxnet.carconfigurator.network.data.ApiResponse
import com.kacera.thinxnet.carconfigurator.persistance.ConfigurationStore
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfigurationImageRepository
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfiguratorRepository
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.ModelsAdapter
import com.kacera.thinxnet.carconfigurator.ui.configuration.decoration.MarginItemDecoration
import com.kacera.thinxnet.carconfigurator.utils.DepthPageTransformer
import com.kacera.thinxnet.carconfigurator.utils.RetryListener
import com.kacera.thinxnet.carconfigurator.utils.SingleLiveEvent
import com.kacera.thinxnet.carconfigurator.utils.asMutable
import kotlinx.coroutines.launch

class ModelsFragmentViewModel(
    private val carConfiguratorRepository: CarConfiguratorRepository,
    carConfigurationImageRepository: CarConfigurationImageRepository,
    private val configurationStore: ConfigurationStore,
    private val makerId: Long,
    private val modelId: Long
) : ViewModel(), DefaultLifecycleObserver, RetryListener {

    val adapter: ModelsAdapter = ModelsAdapter(carConfigurationImageRepository) {
        configurationStore.saveConfiguration(it)
    }

    val transformer = DepthPageTransformer()
    val offscreenLimit = 1
    val selectedItem: LiveData<Int> = SingleLiveEvent()
    val decorations = listOf<ItemDecoration>(
        MarginItemDecoration(
            R.dimen.viewPagerMargin,
            MarginItemDecoration.MARGIN_LEFT or MarginItemDecoration.MARGIN_RIGHT or MarginItemDecoration.MARGIN_BOTTOM
        )
    )

    val apiError: LiveData<Boolean> =
        Transformations.distinctUntilChanged(MutableLiveData(false))
    val loading: LiveData<Boolean> =
        Transformations.distinctUntilChanged(MutableLiveData(true))

    override fun onCreate(owner: LifecycleOwner) {
        loadModels()
    }

    override fun onRetry() {
        loadModels()
    }

    private fun loadModels() {
        viewModelScope.launch {
            loading.asMutable().value = true
            apiError.asMutable().value = false
            when (val response = carConfiguratorRepository.getModels(makerId)) {
                is ApiResponse.Success -> {
                    adapter.setItems(response.result)
                    if (modelId > 0) {
                        selectedItem.asMutable().value =
                            response.result.map { it.id }.indexOf(modelId)
                    }
                }
                is ApiResponse.Error -> {
                    apiError.asMutable().value = true
                }
            }
            loading.asMutable().value = false
        }
    }
}