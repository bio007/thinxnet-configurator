package com.kacera.thinxnet.carconfigurator.ui.preview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.kacera.thinxnet.carconfigurator.databinding.LayoutFragmentCarPreviewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ConfigurationPreviewFragment : Fragment() {

    companion object {
        const val KEY_CONFIGURATION = "configuration"
    }

    private val viewModel by viewModel<ConfigurationPreviewFragmentViewModel> {
        parametersOf(arguments?.getParcelable(KEY_CONFIGURATION))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutFragmentCarPreviewBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = this@ConfigurationPreviewFragment.viewModel
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.editConfiguration.observe(viewLifecycleOwner, Observer {
            it.navigate(parentFragmentManager)
        })
    }
}