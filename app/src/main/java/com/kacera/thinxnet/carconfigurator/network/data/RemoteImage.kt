package com.kacera.thinxnet.carconfigurator.network.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RemoteImage(
    @Json(name = "urls") val urls: UrlMap
)

@JsonClass(generateAdapter = true)
data class UrlMap(
    @Json(name = "regular") val regularUrl: String,
    @Json(name = "full") val fullUrl: String,
    @Json(name = "small") val smallUrl: String
)