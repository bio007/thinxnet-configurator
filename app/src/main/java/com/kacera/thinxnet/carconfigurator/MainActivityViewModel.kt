package com.kacera.thinxnet.carconfigurator

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kacera.thinxnet.carconfigurator.persistance.ConfigurationStore
import com.kacera.thinxnet.carconfigurator.persistance.data.CarConfiguration
import com.kacera.thinxnet.carconfigurator.ui.navigation.NavigationTarget
import com.kacera.thinxnet.carconfigurator.utils.Transformations

class MainActivityViewModel(
    store: ConfigurationStore
) : ViewModel() {

    val userConfiguration: LiveData<NavigationTarget> =
        Transformations.mapSingleEvent(store.getConfiguration()) {
            if (it == CarConfiguration.EMPTY)
                NavigationTarget.EmptyConfigurationFragmentTarget
            else
                NavigationTarget.CarFragmentTarget(it).clearBackStack()
        }
}