package com.kacera.thinxnet.carconfigurator.utils

import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.kacera.thinxnet.carconfigurator.R

private const val MIN_SCALE = 0.75f

/**
 * Modified DepthPageTransformer class from official Android Developer portal
 */

class DepthPageTransformer : ViewPager2.PageTransformer {

    override fun transformPage(view: View, position: Float) {
        view.apply {
            when {
                position < -1 -> { // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    alpha = 0f
                }
                position <= 0 -> { // [-1,0]
                    // Use the default slide transition when moving to the left page
                    alpha = 1f
                    translationX = 0f
                    translationZ = 0f
                    scaleX = 1f
                    scaleY = 1f
                }
                position <= 1 -> { // (0,1]
                    // Fade the page out.
                    alpha = Math.max(1 - position, 0.3f)

                    // Counteract the default slide transition
                    val space =
                        ((width - (width * MIN_SCALE)) / 2f) + resources.getDimensionPixelSize(R.dimen.viewPagerMargin) * 1.5f
                    translationX = space * -position
                    // Move it behind the left page
                    translationZ = -1f

                    // Scale the page down (between MIN_SCALE and 1)
                    val scaleFactor = (MIN_SCALE + (1 - MIN_SCALE) * (1 - Math.abs(position)))
                    scaleX = scaleFactor
                    scaleY = scaleFactor
                }
                else -> { // (1,+Infinity]
                    // This page is way off-screen to the right.
                    alpha = 0f
                }
            }
        }
    }
}