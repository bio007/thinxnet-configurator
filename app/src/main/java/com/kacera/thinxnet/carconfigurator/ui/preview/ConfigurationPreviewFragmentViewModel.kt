package com.kacera.thinxnet.carconfigurator.ui.preview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kacera.thinxnet.carconfigurator.R
import com.kacera.thinxnet.carconfigurator.persistance.data.CarConfiguration
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfigurationImageRepository
import com.kacera.thinxnet.carconfigurator.ui.navigation.NavigationTarget
import com.kacera.thinxnet.carconfigurator.utils.DynamicRequestCreator
import com.kacera.thinxnet.carconfigurator.utils.DynamicRequestCreator.DrawableRequestCreator
import com.kacera.thinxnet.carconfigurator.utils.DynamicRequestCreator.PicassoRequestCreator
import com.kacera.thinxnet.carconfigurator.utils.SingleLiveEvent
import com.kacera.thinxnet.carconfigurator.utils.asMutable
import kotlinx.coroutines.launch

class ConfigurationPreviewFragmentViewModel(
    private val configuration: CarConfiguration,
    private val imageRepository: CarConfigurationImageRepository
) : ViewModel() {

    val editConfiguration: LiveData<NavigationTarget> = SingleLiveEvent()

    val maker = configuration.maker
    val model = configuration.model
    val fuel = configuration.fuelType
    val modelImage: LiveData<DynamicRequestCreator> =
        MutableLiveData(DrawableRequestCreator(R.drawable.anim_logo_loading))

    init {
        viewModelScope.launch {
            val url = configuration.url
                ?: imageRepository.getCarModelImage("${configuration.maker} ${configuration.model}")

            modelImage.asMutable().value =
                PicassoRequestCreator(imageRepository.getImageRequest(url), R.drawable.anim_logo_loading)
        }
    }

    fun onEditMaker() {
        editConfiguration.asMutable().value =
            NavigationTarget.MakersFragmentTarget.addToBackStack("makers")
    }

    fun onEditModel() {
        editConfiguration.asMutable().value =
            NavigationTarget.ModelsFragmentTarget(configuration.makerId).addToBackStack("models")
    }

    fun onEditFuel() {
        editConfiguration.asMutable().value =
            NavigationTarget.ModelsFragmentTarget(configuration.makerId, configuration.modelId)
                .addToBackStack("models")
    }
}