package com.kacera.thinxnet.carconfigurator.ui.navigation

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.kacera.thinxnet.carconfigurator.R
import com.kacera.thinxnet.carconfigurator.persistance.data.CarConfiguration
import com.kacera.thinxnet.carconfigurator.ui.configuration.MakersFragment
import com.kacera.thinxnet.carconfigurator.ui.configuration.ModelsFragment
import com.kacera.thinxnet.carconfigurator.ui.configuration.ModelsFragment.Companion.KEY_MAKER
import com.kacera.thinxnet.carconfigurator.ui.configuration.ModelsFragment.Companion.KEY_MODEL
import com.kacera.thinxnet.carconfigurator.ui.preview.ConfigurationPreviewFragment
import com.kacera.thinxnet.carconfigurator.ui.preview.ConfigurationPreviewFragment.Companion.KEY_CONFIGURATION
import com.kacera.thinxnet.carconfigurator.ui.preview.EmptyConfigurationFragment

sealed class NavigationTarget {

    private var clearBackStack = false
    private var backStackTag: String? = null

    protected abstract fun navigateInternal(transaction: FragmentTransaction): FragmentTransaction

    fun navigate(fragmentManager: FragmentManager) {
        if (clearBackStack && fragmentManager.backStackEntryCount > 0) {
            fragmentManager.popBackStack(
                fragmentManager.getBackStackEntryAt(0).id,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            )
        }

        val transaction = navigateInternal(fragmentManager.beginTransaction())
        backStackTag?.let {
            transaction.addToBackStack(it)
        }

        backStackTag = null
        clearBackStack = false
        transaction.commit()
    }

    fun addToBackStack(tag: String) = this.apply {
        backStackTag = tag
    }

    fun clearBackStack() = this.apply {
        clearBackStack = true
    }

    object EmptyConfigurationFragmentTarget : NavigationTarget() {
        override fun navigateInternal(transaction: FragmentTransaction) =
            transaction.replace(
                R.id.fragment_container,
                EmptyConfigurationFragment::class.java,
                null
            )
    }

    class CarFragmentTarget(private val configuration: CarConfiguration) : NavigationTarget() {
        override fun navigateInternal(transaction: FragmentTransaction) =
            transaction.replace(
                R.id.fragment_container,
                ConfigurationPreviewFragment::class.java,
                Bundle().apply { putParcelable(KEY_CONFIGURATION, configuration) }
            )
    }

    object MakersFragmentTarget : NavigationTarget() {
        override fun navigateInternal(transaction: FragmentTransaction) =
            transaction.replace(
                R.id.fragment_container,
                MakersFragment::class.java,
                null
            )
    }

    class ModelsFragmentTarget(private val makerId: Long, private val modelId: Long = -1) : NavigationTarget() {
        override fun navigateInternal(transaction: FragmentTransaction) =
            transaction.replace(
                R.id.fragment_container,
                ModelsFragment::class.java,
                Bundle().apply {
                    putLong(KEY_MAKER, makerId)
                    putLong(KEY_MODEL, modelId)
                }
            )
    }
}