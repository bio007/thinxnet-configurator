package com.kacera.thinxnet.carconfigurator.ui.configuration.data

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.kacera.thinxnet.carconfigurator.databinding.LayoutMakerItemBinding
import com.kacera.thinxnet.carconfigurator.network.data.Maker
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MakersAdapter(
    private val imageRepository: CarConfigurationImageRepository,
    private val clickListener: (Long) -> Unit
) : BaseAdapter<Maker, MakersAdapter.ViewHolder>() {

    init {
        setHasStableIds(true)
    }

    override fun getDiffCallback(oldItems: List<Maker>, newItems: List<Maker>): DiffUtil.Callback =
        DiffCallback(oldItems, newItems)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutMakerItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )


    override fun getItemId(position: Int): Long {
        return items[position].id
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(private val binding: LayoutMakerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener { clickListener(getItemId(adapterPosition)) }
        }

        fun bind(item: Maker) {
            binding.logo.setImageDrawable(getPlaceholder(binding.root.context))
            binding.title.text = item.name

            GlobalScope.launch(Dispatchers.Main) {
                val url = imageRepository.getCarMakerLogo(item.name)
                val imageLoader = imageRepository.getImageRequest(url)
                    .transform(CropCircleTransformation())
                    .noPlaceholder()    //I handle placeholders myself as I load url from unsplash first
                imageLoader.into(binding.logo)
            }
        }
    }

    private class DiffCallback(
        private val oldList: List<Maker>,
        private val newList: List<Maker>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].id == newList[newItemPosition].id

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]
    }
}