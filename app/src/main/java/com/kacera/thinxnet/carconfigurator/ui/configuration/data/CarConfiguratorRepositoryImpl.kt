package com.kacera.thinxnet.carconfigurator.ui.configuration.data

import com.kacera.thinxnet.carconfigurator.network.ConfiguratorApi
import com.kacera.thinxnet.carconfigurator.network.data.ApiResponse
import com.kacera.thinxnet.carconfigurator.network.data.Maker
import com.kacera.thinxnet.carconfigurator.network.data.Model

class CarConfiguratorRepositoryImpl(private val configuratorApi: ConfiguratorApi) : CarConfiguratorRepository {
    override suspend fun getMakers(): ApiResponse<List<Maker>> {
        return try {
            ApiResponse.Success(configuratorApi.getMakers().makers)
        } catch (e: Throwable) {
            ApiResponse.Error(e)
        }
    }

    override suspend fun getModels(makerId: Long): ApiResponse<List<Model>> {
        return try {
            ApiResponse.Success(configuratorApi.getModels(makerId).models)
        } catch (e: Throwable) {
            ApiResponse.Error(e)
        }
    }
}