package com.kacera.thinxnet.carconfigurator.utils

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2

@BindingAdapter("visible")
fun View.visible(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter("itemDecoration")
fun ViewPager2.setItemDecoration(itemDecorations: List<RecyclerView.ItemDecoration>) {
    itemDecorations.forEach {
        addItemDecoration(it)
    }
}

@BindingAdapter("itemDecoration")
fun RecyclerView.setItemDecoration(itemDecorations: List<RecyclerView.ItemDecoration>) {
    itemDecorations.forEach {
        addItemDecoration(it)
    }
}

@BindingAdapter("android:src")
fun ImageView.setImage(requestCreator: DynamicRequestCreator) {
    requestCreator.loadImage(this)
}
