package com.kacera.thinxnet.carconfigurator.di

import com.kacera.thinxnet.carconfigurator.MainActivityViewModel
import com.kacera.thinxnet.carconfigurator.network.ConfiguratorApi
import com.kacera.thinxnet.carconfigurator.network.UnsplashApi
import com.kacera.thinxnet.carconfigurator.persistance.ConfigurationStore
import com.kacera.thinxnet.carconfigurator.persistance.SharedPreferencesConfigurationStore
import com.kacera.thinxnet.carconfigurator.persistance.data.CarConfiguration
import com.kacera.thinxnet.carconfigurator.ui.configuration.MakersFragmentViewModel
import com.kacera.thinxnet.carconfigurator.ui.configuration.ModelsFragmentViewModel
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfigurationImageRepository
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfigurationImageRepositoryImpl
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfiguratorRepository
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfiguratorRepositoryImpl
import com.kacera.thinxnet.carconfigurator.ui.preview.ConfigurationPreviewFragmentViewModel
import com.kacera.thinxnet.carconfigurator.ui.preview.EmptyConfigurationFragmentViewModel
import com.squareup.moshi.Moshi
import com.squareup.picasso.Picasso
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val appModule = module {

    single<ConfigurationStore> {
        SharedPreferencesConfigurationStore(get())
    }

    single<CarConfiguratorRepository> {
        CarConfiguratorRepositoryImpl(get())
    }

    single<CarConfigurationImageRepository> {
        CarConfigurationImageRepositoryImpl(get(), get())
    }

    viewModel {
        MainActivityViewModel(get())
    }

    viewModel {
        EmptyConfigurationFragmentViewModel()
    }

    viewModel {(configuration: CarConfiguration) ->
        ConfigurationPreviewFragmentViewModel(configuration, get())
    }

    viewModel {
        MakersFragmentViewModel(get(), get())
    }

    viewModel { (makerId: Long, modelId: Long) ->
        ModelsFragmentViewModel(get(), get(), get(), makerId, modelId)
    }

    single {
        Picasso.get()
    }

    single<Converter.Factory> {
        MoshiConverterFactory.create(
            Moshi.Builder()
                .build()
        )
    }

    single {
        Retrofit.Builder()
            .baseUrl(UnsplashApi.BASE_URL)
            .addConverterFactory(get())
            .build()
            .create(UnsplashApi::class.java)
    }

    single {
        Retrofit.Builder()
            .baseUrl(ConfiguratorApi.BASE_URL)
            .addConverterFactory(get())
            .build()
            .create(ConfiguratorApi::class.java)
    }
}