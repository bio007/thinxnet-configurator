package com.kacera.thinxnet.carconfigurator.ui.preview

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kacera.thinxnet.carconfigurator.ui.navigation.NavigationTarget
import com.kacera.thinxnet.carconfigurator.utils.SingleLiveEvent
import com.kacera.thinxnet.carconfigurator.utils.asMutable

class EmptyConfigurationFragmentViewModel : ViewModel() {

    val createConfiguration: LiveData<NavigationTarget> = SingleLiveEvent()

    fun onCreateClick() {
        createConfiguration.asMutable().value =
            NavigationTarget.MakersFragmentTarget.addToBackStack("makers")
    }
}