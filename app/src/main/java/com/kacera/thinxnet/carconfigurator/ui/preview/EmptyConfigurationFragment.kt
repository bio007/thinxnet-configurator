package com.kacera.thinxnet.carconfigurator.ui.preview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.kacera.thinxnet.carconfigurator.databinding.LayoutFragmentEmptyConfigurationBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class EmptyConfigurationFragment : Fragment() {

    private val viewModel by viewModel<EmptyConfigurationFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return with(LayoutFragmentEmptyConfigurationBinding.inflate(inflater, container, false)) {
            lifecycleOwner = viewLifecycleOwner
            viewModel = this@EmptyConfigurationFragment.viewModel
                root
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.createConfiguration.observe(viewLifecycleOwner, Observer {
            it.navigate(parentFragmentManager)
        })
    }
}