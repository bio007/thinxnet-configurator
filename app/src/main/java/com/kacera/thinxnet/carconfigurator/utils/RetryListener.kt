package com.kacera.thinxnet.carconfigurator.utils

interface RetryListener {
    fun onRetry()
}