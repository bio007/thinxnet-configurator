package com.kacera.thinxnet.carconfigurator.network.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Model(
    @Json(name = "id") val id: Long,
    @Json(name = "model") val model: String,
    @Json(name = "makeId") val makerId: Long,
    @Json(name = "make") val makerName: String,
    @Json(name = "fuelTypes") val fuelTypes: List<FuelType>
)

@JsonClass(generateAdapter = true)
data class Models(@Json(name = "data") val models: List<Model>)
