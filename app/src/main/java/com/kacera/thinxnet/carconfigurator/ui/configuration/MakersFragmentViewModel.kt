package com.kacera.thinxnet.carconfigurator.ui.configuration

import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.kacera.thinxnet.carconfigurator.R
import com.kacera.thinxnet.carconfigurator.network.data.ApiResponse
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfigurationImageRepository
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfiguratorRepository
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.MakersAdapter
import com.kacera.thinxnet.carconfigurator.ui.configuration.decoration.MarginItemDecoration
import com.kacera.thinxnet.carconfigurator.ui.navigation.NavigationTarget
import com.kacera.thinxnet.carconfigurator.utils.RetryListener
import com.kacera.thinxnet.carconfigurator.utils.SingleLiveEvent
import com.kacera.thinxnet.carconfigurator.utils.asMutable
import kotlinx.coroutines.launch

class MakersFragmentViewModel(
    private val carConfiguratorRepository: CarConfiguratorRepository,
    carConfigurationImageRepository: CarConfigurationImageRepository
) : ViewModel(), DefaultLifecycleObserver, RetryListener {

    val maker: LiveData<NavigationTarget> = SingleLiveEvent()

    val adapter: MakersAdapter = MakersAdapter(carConfigurationImageRepository) {
        maker.asMutable().value = NavigationTarget.ModelsFragmentTarget(it).addToBackStack("models")
    }
    val decorations = listOf<ItemDecoration>(MarginItemDecoration(R.dimen.recyclerViewMargin))

    val apiError: LiveData<Boolean> =
        Transformations.distinctUntilChanged(MutableLiveData(false))
    val loading: LiveData<Boolean> =
        Transformations.distinctUntilChanged(MutableLiveData(true))

    override fun onCreate(owner: LifecycleOwner) {
        loadConfiguration()
    }

    override fun onRetry() {
        loadConfiguration()
    }

    private fun loadConfiguration() {
        viewModelScope.launch {
            loading.asMutable().value = true
            apiError.asMutable().value = false
            when (val response = carConfiguratorRepository.getMakers()) {
                is ApiResponse.Success -> {
                    adapter.setItems(response.result)
                }
                is ApiResponse.Error -> {
                    apiError.asMutable().value = true
                }
            }
            loading.asMutable().value = false
        }
    }
}