package com.kacera.thinxnet.carconfigurator.network.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FuelType(
    @Json(name = "fuelTypeId") val id: Long,
    @Json(name = "displayName") val name: String
) {
    override fun toString(): String {
        return name
    }
}