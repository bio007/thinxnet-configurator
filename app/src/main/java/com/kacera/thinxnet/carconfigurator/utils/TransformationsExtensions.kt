package com.kacera.thinxnet.carconfigurator.utils

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import java.util.concurrent.atomic.AtomicBoolean

object Transformations {

    fun <X, Y> mapSingleEvent(source: LiveData<X>, mapFunction: (X) -> Y): LiveData<Y> {

        return object : MediatorLiveData<Y>() {
            private val mPending = AtomicBoolean(false)

            @MainThread
            override fun setValue(value: Y?) {
                mPending.set(true)
                super.setValue(value)
            }

            @MainThread
            override fun observe(owner: LifecycleOwner, observer: Observer<in Y>) {
                super.observe(owner, Observer { t ->
                    if (mPending.compareAndSet(true, false)) {
                        observer.onChanged(t)
                    }
                })
            }
        }.apply {
            addSource(source) { x -> setValue(mapFunction(x)) }
        }
    }
}
