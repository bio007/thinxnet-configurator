package com.kacera.thinxnet.carconfigurator.ui.configuration.data

import android.content.Context
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.kacera.thinxnet.carconfigurator.R
import com.kacera.thinxnet.carconfigurator.utils.getDrawableWithAnimation

abstract class BaseAdapter<I, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {

    protected val items = mutableListOf<I>()

    protected abstract fun getDiffCallback(oldItems: List<I>, newItems: List<I>): DiffUtil.Callback

    fun setItems(items: List<I>) {
        DiffUtil.calculateDiff(getDiffCallback(this.items, items)).also {
            it.dispatchUpdatesTo(this)
        }

        this.items.apply {
            clear()
            addAll(items)
        }
    }

    override fun getItemCount() = items.size

    protected fun getPlaceholder(context: Context) =
        context.getDrawableWithAnimation(R.drawable.anim_logo_loading)
}