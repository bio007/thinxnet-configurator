package com.kacera.thinxnet.carconfigurator.ui.configuration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.kacera.thinxnet.carconfigurator.databinding.LayoutFragmentModelsBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ModelsFragment : Fragment() {

    companion object {
        const val KEY_MAKER = "makerId"
        const val KEY_MODEL = "modelId"
    }

    private val viewModel by viewModel<ModelsFragmentViewModel> {
        parametersOf(arguments?.getLong(KEY_MAKER), arguments?.getLong(KEY_MODEL))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycle.addObserver(viewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutFragmentModelsBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = this@ModelsFragment.viewModel
        }.root
    }
}