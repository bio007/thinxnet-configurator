package com.kacera.thinxnet.carconfigurator.ui.configuration.decoration

import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView

class MarginItemDecoration(@DimenRes private val margin: Int, private val flags: Int = MARGIN_ALL) : RecyclerView.ItemDecoration() {

    companion object {
        const val MARGIN_TOP = 0x1
        const val MARGIN_BOTTOM = 0x2
        const val MARGIN_LEFT = 0x4
        const val MARGIN_RIGHT = 0x8
        const val MARGIN_ALL = MARGIN_TOP or MARGIN_BOTTOM or MARGIN_LEFT or MARGIN_RIGHT
    }

    private var marginPx: Int? = null

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val margin = this.marginPx
            ?: parent.resources.getDimensionPixelSize(margin).also {
                this.marginPx = it
            }

        outRect.apply {
            if (flags and MARGIN_TOP == MARGIN_TOP) top = margin
            if (flags and MARGIN_LEFT == MARGIN_LEFT) left = margin
            if (flags and MARGIN_RIGHT == MARGIN_RIGHT) right = margin
            if (flags and MARGIN_BOTTOM == MARGIN_BOTTOM) bottom = margin
        }
    }
}