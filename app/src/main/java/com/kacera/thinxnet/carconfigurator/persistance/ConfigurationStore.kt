package com.kacera.thinxnet.carconfigurator.persistance

import androidx.lifecycle.LiveData
import com.kacera.thinxnet.carconfigurator.persistance.data.CarConfiguration

interface ConfigurationStore {
    fun getConfiguration(): LiveData<CarConfiguration>
    fun saveConfiguration(configuration: CarConfiguration)
}