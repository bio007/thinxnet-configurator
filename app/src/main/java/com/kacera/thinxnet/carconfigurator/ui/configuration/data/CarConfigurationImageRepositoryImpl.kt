package com.kacera.thinxnet.carconfigurator.ui.configuration.data

import android.content.res.Configuration
import android.net.Uri
import androidx.collection.LruCache
import com.kacera.thinxnet.carconfigurator.R
import com.kacera.thinxnet.carconfigurator.network.UnsplashApi
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator

private const val LOGO_KEYWORD = "emblem"

class CarConfigurationImageRepositoryImpl(
    private val imageApi: UnsplashApi,
    private val picasso: Picasso
) : CarConfigurationImageRepository {

    private val cache: LruCache<String, String> = LruCache(1000)

    override suspend fun getCarMakerLogo(makerName: String): Uri? =
        try {
            val query = "$makerName $LOGO_KEYWORD"
            Uri.parse(cache.get(query) ?: imageApi.getImage(query).urls.smallUrl.also {
                cache.put(
                    query,
                    it
                )
            })
        } catch (e: Throwable) {
            null
        }

    override suspend fun getCarModelImage(modelName: String, orientation: Int): Uri? =
        try {
            Uri.parse(cache.get(modelName) ?: imageApi.getImage(
                modelName,
                when (orientation) {
                    Configuration.ORIENTATION_PORTRAIT -> "portrait"
                    Configuration.ORIENTATION_LANDSCAPE -> "landscape"
                    else -> null
                }
            ).urls.regularUrl.also { cache.put(modelName, it) })
        } catch (e: Throwable) {
            null
        }

    override suspend fun getImageRequest(url: Uri?): RequestCreator =
        if (url != null) {
            picasso.load(url).error(R.drawable.ic_api_error)
        } else {
            picasso.load(R.drawable.ic_api_error).error(R.drawable.ic_api_error)
        }
}