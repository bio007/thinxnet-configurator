package com.kacera.thinxnet.carconfigurator.ui.configuration.data

import android.content.res.Configuration
import android.net.Uri
import com.squareup.picasso.RequestCreator

interface CarConfigurationImageRepository {

    suspend fun getCarMakerLogo(makerName: String): Uri?
    suspend fun getCarModelImage(modelName: String, orientation: Int = Configuration.ORIENTATION_UNDEFINED): Uri?

    suspend fun getImageRequest(url: Uri?): RequestCreator
}