package com.kacera.thinxnet.carconfigurator.ui.configuration.data

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.kacera.thinxnet.carconfigurator.databinding.LayoutModelItemBinding
import com.kacera.thinxnet.carconfigurator.network.data.FuelType
import com.kacera.thinxnet.carconfigurator.network.data.Model
import com.kacera.thinxnet.carconfigurator.persistance.data.CarConfiguration
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ModelsAdapter(
    private val imageRepository: CarConfigurationImageRepository,
    private val clickListener: (CarConfiguration) -> Unit
) : BaseAdapter<Model, ModelsAdapter.ViewHolder>() {

    init {
        setHasStableIds(true)
    }

    override fun getDiffCallback(oldItems: List<Model>, newItems: List<Model>): DiffUtil.Callback =
        DiffCallback(oldItems, newItems)

    override fun getItemId(position: Int) = items[position].id

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutModelItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(private val binding: LayoutModelItemBinding) : RecyclerView.ViewHolder(binding.root) {

        private var url: Uri? = null
        private val spinnerAdapter: ArrayAdapter<FuelType> =
            ArrayAdapter(binding.root.context, android.R.layout.simple_dropdown_item_1line, mutableListOf<FuelType>())

        init {
            binding.confirmButton.setOnClickListener { clickListener(with(items[adapterPosition]) {
                val selectedFuel = (binding.fuelType.selectedItem as FuelType)
                CarConfiguration(makerName, makerId, model, id, selectedFuel.name, selectedFuel.id, url)
            }) }
            binding.fuelType.adapter = spinnerAdapter
        }

        fun bind(item: Model) {
            val modelName = "${item.makerName} ${item.model}"

            binding.modelImage.setImageDrawable(getPlaceholder(binding.root.context))
            binding.title.text = modelName
            spinnerAdapter.clear()
            spinnerAdapter.addAll(item.fuelTypes)

            GlobalScope.launch(Dispatchers.Main) {
                url = imageRepository.getCarModelImage(modelName, binding.modelImage.resources.configuration.orientation)
                val imageLoader = imageRepository.getImageRequest(url)
                    .transform(RoundedCornersTransformation(80, 10))
                    .noPlaceholder()    //I handle placeholders myself as I load url from unsplash first
                imageLoader.into(binding.modelImage)
            }
        }
    }

    private class DiffCallback(
        private val oldList: List<Model>,
        private val newList: List<Model>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].id == newList[newItemPosition].id

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]
    }
}