package com.kacera.thinxnet.carconfigurator.network.data

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Maker(
    @Json(name = "id") val id: Long,
    @Json(name = "make") val name: String
) : Parcelable

@JsonClass(generateAdapter = true)
data class Makers(@Json(name = "data") val makers: List<Maker>)