package com.kacera.thinxnet.carconfigurator.persistance

import android.content.Context
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import com.kacera.thinxnet.carconfigurator.persistance.data.CarConfiguration

private const val KEY_MAKER_ID = "makerId"
private const val KEY_MAKER_NAME = "makerName"
private const val KEY_MODEL_ID = "modelId"
private const val KEY_MODEL = "model"
private const val KEY_FUEL_TYPE_ID = "fuelTypeId"
private const val KEY_FUEL_TYPE = "fuelType"
private const val KEY_IMAGE_URL = "image_url"

class SharedPreferencesConfigurationStore(context: Context) : ConfigurationStore {
    private val sharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context.applicationContext)

    private val configuration = MutableLiveData<CarConfiguration>()

    override fun getConfiguration(): LiveData<CarConfiguration> {
        val makerId = sharedPreferences.getLong(KEY_MAKER_ID, -1)
        val makerName = sharedPreferences.getString(KEY_MAKER_NAME, null)
        val modelId = sharedPreferences.getLong(KEY_MODEL_ID, -1)
        val model = sharedPreferences.getString(KEY_MODEL, null)
        val fuelTypeId = sharedPreferences.getLong(KEY_FUEL_TYPE_ID, -1)
        val fuelType = sharedPreferences.getString(KEY_FUEL_TYPE, null)
        val imageUrl = sharedPreferences.getString(KEY_IMAGE_URL, null)?.let {
            Uri.parse(it)
        }

        configuration.value = if (makerId > 0
            && modelId > 0
            && fuelTypeId > 0
            && makerName != null
            && model != null
            && fuelType != null
        )
            CarConfiguration(makerName, makerId, model, modelId, fuelType, fuelTypeId, imageUrl)
        else
            CarConfiguration.EMPTY

        return configuration
    }

    override fun saveConfiguration(configuration: CarConfiguration) {
        sharedPreferences.edit().apply {
            putLong(KEY_MAKER_ID, configuration.makerId)
            putString(KEY_MAKER_NAME, configuration.maker)
            putLong(KEY_MODEL_ID, configuration.modelId)
            putString(KEY_MODEL, configuration.model)
            putLong(KEY_FUEL_TYPE_ID, configuration.fuelTypeId)
            putString(KEY_FUEL_TYPE, configuration.fuelType)
            putString(KEY_IMAGE_URL, configuration.url?.toString())
        }.apply()

        this.configuration.value = configuration
    }
}