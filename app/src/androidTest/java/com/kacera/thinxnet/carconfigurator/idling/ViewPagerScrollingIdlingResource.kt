package com.kacera.thinxnet.carconfigurator.idling

import android.app.Activity
import android.view.View
import androidx.annotation.IdRes
import androidx.viewpager2.widget.ViewPager2

class ViewPagerScrollingIdlingResource(
    activity: Activity,
    @IdRes private val viewId: Int
) : BaseIdlingResource(activity) {

    private val view: View?
        get() = activity.findViewById(viewId)

    override fun getName(): String = "ViewPagerScrollingIdlingResource"

    override fun isIdle(): Boolean = view?.let {
        it !is ViewPager2 || it.scrollState == ViewPager2.SCROLL_STATE_IDLE
    } ?: false
}