package com.kacera.thinxnet.carconfigurator.idling

import android.app.Activity
import android.widget.ImageView
import androidx.annotation.IdRes

class RemoteImageLoadedIdlingResource(
    activity: Activity,
    @IdRes private val imageViewId: Int
) : BaseIdlingResource(activity) {

    private val imageView: ImageView
        get() = activity.findViewById(imageViewId)

    override fun isIdle() = imageView.drawable != null

    override fun getName() = "RemoteImageLoadedIdlingResource"
}