package com.kacera.thinxnet.carconfigurator.matcher

import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.PorterDuff
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.Px
import androidx.core.content.ContextCompat
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

fun withDrawable(@DrawableRes resourceId: Int, @ColorInt tintColor: Int? = null): Matcher<View> =
    DrawableMatcher(resourceId, tintColor)

fun containsDrawable(): Matcher<View> = ContainsDrawableMatcher()

class ContainsDrawableMatcher : TypeSafeMatcher<View>() {
    override fun matchesSafely(view: View): Boolean {
        return view is ImageView && view.drawable != null
    }

    override fun describeTo(description: Description) {
        with(description) {
            appendText("ImageView with any drawable set")
        }
    }
}

class DrawableMatcher(
    @DrawableRes private val it: Int,
    @ColorRes private val tint: Int? = null,
    private val tintMode: PorterDuff.Mode = PorterDuff.Mode.SRC_IN
) : TypeSafeMatcher<View>() {

    override fun matchesSafely(view: View): Boolean {
        val context = view.context
        val tintColor = tint?.let { ContextCompat.getColor(context, it) }
        val expectedBitmap = context.getDrawable(it)?.tinted(tintColor, tintMode)?.toBitmap()

        return view is ImageView && view.drawable.toBitmap().sameAs(expectedBitmap)
    }

    override fun describeTo(description: Description) {
        with(description) {
            appendText("ImageView with drawable same as drawable with id $it")
            tint?.let { appendText(", tint color id: $it, mode: $tintMode") }
        }
    }
}

private fun Drawable.tinted(
    @ColorInt tintColor: Int? = null,
    tintMode: PorterDuff.Mode = PorterDuff.Mode.SRC_IN
) =
    apply {
        setTintList(tintColor?.let { ColorStateList.valueOf(it) })
        setTintMode(tintMode)
    }

private fun Drawable.toBitmap(
    @Px width: Int = intrinsicWidth,
    @Px height: Int = intrinsicHeight,
    config: Bitmap.Config? = null
): Bitmap {
    if (this is BitmapDrawable) {
        if (config == null || bitmap.config == config) {
            // Fast-path to return original. Bitmap.createScaledBitmap will do this check, but it
            // involves allocation and two jumps into native code so we perform the check ourselves.
            if (width == intrinsicWidth && height == intrinsicHeight) {
                return bitmap
            }
            return Bitmap.createScaledBitmap(bitmap, width, height, true)
        }
    }

    val oldBounds = bounds
    val bitmap = Bitmap.createBitmap(width, height, config ?: Bitmap.Config.ARGB_8888)
    setBounds(0, 0, width, height)
    draw(Canvas(bitmap))

    setBounds(oldBounds.left, oldBounds.top, oldBounds.right, oldBounds.bottom)
    return bitmap
}