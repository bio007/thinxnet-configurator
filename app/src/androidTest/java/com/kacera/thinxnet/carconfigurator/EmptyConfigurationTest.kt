package com.kacera.thinxnet.carconfigurator

import androidx.preference.PreferenceManager
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.kacera.thinxnet.carconfigurator.robot.emptyConfiguration
import com.kacera.thinxnet.carconfigurator.rule.CarConfigurationTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EmptyConfigurationTest {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @get:Rule
    val configurationRule =
        CarConfigurationTestRule(
            PreferenceManager.getDefaultSharedPreferences(InstrumentationRegistry.getInstrumentation().targetContext),
            null
        )

    @Test
    fun onCreateConfiguration() {
        emptyConfiguration(activityRule.activity) {
            clickCreate()
            makerScreenIsVisible()
        }
    }
}
