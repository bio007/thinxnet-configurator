package com.kacera.thinxnet.carconfigurator.robot

import com.kacera.thinxnet.carconfigurator.MainActivity
import com.kacera.thinxnet.carconfigurator.R
import com.kacera.thinxnet.carconfigurator.idling.RemoteImageLoadedIdlingResource
import com.kacera.thinxnet.carconfigurator.idling.withIdlingResource

fun previewConfiguration(activity: MainActivity, func: PreviewConfigurationRobot.() -> Unit) =
    PreviewConfigurationRobot(activity).apply(func)

class PreviewConfigurationRobot(private val activity: MainActivity) : BaseRobot(activity) {

    fun isImageLoaded() {
        withIdlingResource(RemoteImageLoadedIdlingResource(activity, R.id.model_image)) {
            isViewDisplayed(R.id.model_image)
            imageViewContainsDrawable(R.id.model_image)
        }
    }

    fun onEditFuelClicked(func: ModelConfigurationRobot.() -> Unit) {
        clickOnView(R.id.fuel_type_edit)
        modelConfiguration(activity, func)
    }

    fun onEditModelClicked(func: ModelConfigurationRobot.() -> Unit) {
        clickOnView(R.id.model_edit)
        modelConfiguration(activity, func)
    }

    fun onEditMakerClicked(func: ModelConfigurationRobot.() -> Unit) {
        clickOnView(R.id.maker_edit)
        modelConfiguration(activity, func)
    }

    fun isMaker(makerName: String) {
        isViewDisplayed(R.id.maker_name)
        viewContainsText(R.id.maker_name, makerName)
    }

    fun isModel(modelName: String) {
        isViewDisplayed(R.id.model_name)
        viewContainsText(R.id.model_name, modelName)
    }

    fun isNotMaker(modelName: String) {
        isViewDisplayed(R.id.model_name)
        viewNotContainsText(R.id.model_name, modelName)
    }

    fun isNotModel(modelName: String) {
        isViewDisplayed(R.id.model_name)
        viewNotContainsText(R.id.model_name, modelName)
    }

    fun isFuel(fuelName: String) {
        isViewDisplayed(R.id.fuel_type)
        viewContainsText(R.id.fuel_type, fuelName)
    }
}