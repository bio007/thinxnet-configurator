package com.kacera.thinxnet.carconfigurator.idling

import android.app.Activity
import androidx.annotation.IdRes
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2

class ViewPagerDataReceivedIdlingResource(
    activity: Activity,
    @IdRes private val viewId: Int
) : BaseIdlingResource(activity) {

    private val adapter: RecyclerView.Adapter<*>?
        get() = activity.findViewById<ViewPager2>(viewId)?.adapter

    override fun getName(): String = "ViewPagerDataReceivedIdlingResource"

    override fun isIdle(): Boolean = adapter?.let { it.itemCount > 0 } ?: false
}