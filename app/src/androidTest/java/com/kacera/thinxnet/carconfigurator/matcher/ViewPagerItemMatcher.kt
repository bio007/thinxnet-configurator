package com.kacera.thinxnet.carconfigurator.matcher

import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import androidx.core.view.contains
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

fun inActiveViewPagerView(viewPagerMatcher: Matcher<View>): Matcher<View> =
    ViewPagerItemMatcher(viewPagerMatcher)

class ViewPagerItemMatcher(private val viewPagerMatcher: Matcher<View>) : TypeSafeMatcher<View>() {
    override fun matchesSafely(view: View): Boolean {
        var parentView: ViewParent? = view.parent
        while (parentView is View && !viewPagerMatcher.matches(parentView)) {
            parentView = (parentView as View).parent
        }

        if (parentView == null || parentView !is ViewPager2) return false

        return try {
            val field = parentView.javaClass.getDeclaredField("mRecyclerView")
            field.isAccessible = true
            val recyclerView = field.get(parentView) as RecyclerView
            val currentView =
                recyclerView.findViewHolderForAdapterPosition(parentView.currentItem)?.itemView as? ViewGroup
            currentView?.contains(view, true) ?: false
        } catch (e: Exception) {
            false
        }
    }

    override fun describeTo(description: Description) {
        with(description) {
            appendText("View in active ViewPager view")
        }
    }

    private fun ViewGroup.contains(view: View, recursively: Boolean): Boolean {
        if (!recursively) {
            return contains(view)
        } else {
            for (i in 0 until childCount) {
                val child = getChildAt(i)
                if (child === view || (child is ViewGroup && child.contains(view, recursively))) {
                    return true
                }
            }

            return false
        }
    }
}