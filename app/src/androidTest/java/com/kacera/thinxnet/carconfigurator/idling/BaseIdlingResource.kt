package com.kacera.thinxnet.carconfigurator.idling

import android.app.Activity
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource

inline fun withIdlingResource(idlingResource: BaseIdlingResource, registry: IdlingRegistry = IdlingRegistry.getInstance(), block: () -> Unit) {
    registry.register(idlingResource)
    block()
    registry.unregister(idlingResource)
}

abstract class BaseIdlingResource(protected val activity: Activity) : IdlingResource {

    private var callback: IdlingResource.ResourceCallback? = null

    abstract fun isIdle(): Boolean

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?) {
        this.callback = callback
    }

    final override fun isIdleNow(): Boolean {
        if (isIdle()) {
            callback?.onTransitionToIdle()
            return true
        }

        return false
    }
}