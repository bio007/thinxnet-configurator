package com.kacera.thinxnet.carconfigurator.rule

import android.content.SharedPreferences
import com.kacera.thinxnet.carconfigurator.persistance.data.CarConfiguration
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

private const val KEY_MAKER_ID = "makerId"
private const val KEY_MAKER_NAME = "makerName"
private const val KEY_MODEL_ID = "modelId"
private const val KEY_MODEL = "model"
private const val KEY_FUEL_TYPE_ID = "fuelTypeId"
private const val KEY_FUEL_TYPE = "fuelType"
private const val KEY_IMAGE_URL = "image_url"

class CarConfigurationTestRule(
    private val preferences: SharedPreferences,
    private val configuration: CarConfiguration?
) : TestRule {

    override fun apply(base: Statement, description: Description) = object : Statement() {
        override fun evaluate() {
            if (configuration == null) {
                preferences.edit().clear().commit()
            } else {
                preferences.edit().apply {
                    putLong(KEY_MAKER_ID, configuration.makerId)
                    putString(KEY_MAKER_NAME, configuration.maker)
                    putLong(KEY_MODEL_ID, configuration.modelId)
                    putString(KEY_MODEL, configuration.model)
                    putLong(KEY_FUEL_TYPE_ID, configuration.fuelTypeId)
                    putString(KEY_FUEL_TYPE, configuration.fuelType)
                    putString(KEY_IMAGE_URL, configuration.url?.toString())
                }.apply()
            }

            base.evaluate()
        }
    }
}