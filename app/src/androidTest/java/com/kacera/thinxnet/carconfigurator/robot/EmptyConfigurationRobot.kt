package com.kacera.thinxnet.carconfigurator.robot

import com.kacera.thinxnet.carconfigurator.MainActivity
import com.kacera.thinxnet.carconfigurator.R

fun emptyConfiguration(activity: MainActivity, func: EmptyConfigurationRobot.() -> Unit) =
    EmptyConfigurationRobot(activity).apply(func)

class EmptyConfigurationRobot(activity: MainActivity) : BaseRobot(activity) {

    fun clickCreate() {
        clickOnView(R.id.create)
    }

    fun makerScreenIsVisible() {
        viewContainsText(R.id.title, R.string.choose_a_maker)
    }
}