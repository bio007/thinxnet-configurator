package com.kacera.thinxnet.carconfigurator

import android.net.Uri
import androidx.preference.PreferenceManager
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.kacera.thinxnet.carconfigurator.persistance.data.CarConfiguration
import com.kacera.thinxnet.carconfigurator.robot.previewConfiguration
import com.kacera.thinxnet.carconfigurator.rule.CarConfigurationTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class PreviewConfigurationTest {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @get:Rule
    val configurationRule =
        CarConfigurationTestRule(
            PreferenceManager.getDefaultSharedPreferences(InstrumentationRegistry.getInstrumentation().targetContext),
            CarConfiguration(
                "Audi",
                6,
                "A4",
                41,
                "Bioethanol",
                2,
                Uri.parse("https://images.unsplash.com/photo-1527593167147-e9c94a5883e6?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjExOTMyMn0")
            )
        )

    @Test
    fun onConfigurationLoaded() {
        previewConfiguration(activityRule.activity) {
            isMaker("Audi")
            isModel("A4")
            isFuel("Bioethanol")
            isImageLoaded()
        }
    }

    @Test
    fun onEditFuelType() {
        previewConfiguration(activityRule.activity) {
            onEditFuelClicked {
                isModelPageLoaded("Audi A4")
                selectFuelConfiguration("Diesel")
                onConfirmClicked {
                    isMaker("Audi")
                    isModel("A4")
                    isFuel("Diesel")
                }
            }
        }
    }

    @Test
    fun onEditModel() {
        previewConfiguration(activityRule.activity) {
            isModel("A4")
            onEditModelClicked {
                isMakersModelPageLoaded("Audi")
                swipeLeft()
                onConfirmClicked {
                    isMaker("Audi")
                    isNotModel("A4")
                }
            }
        }
    }

    @Test
    fun onEditMaker() {
        previewConfiguration(activityRule.activity) {
            isModel("A4")
            onEditMakerClicked {
                isRecyclerViewLoadedOnPosition(15)
                clickOnRecyclerViewItem(15) {
                    isViewPagerLoaded()
                    onConfirmClicked {
                        isNotMaker("Audi")
                        isNotModel("A4")
                    }
                }
            }
        }
    }
}
