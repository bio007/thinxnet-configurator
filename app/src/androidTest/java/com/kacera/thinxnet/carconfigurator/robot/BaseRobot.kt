package com.kacera.thinxnet.carconfigurator.robot

import android.app.Activity
import android.widget.Button
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import com.kacera.thinxnet.carconfigurator.R
import com.kacera.thinxnet.carconfigurator.idling.RecyclerViewDataReceivedIdlingResource
import com.kacera.thinxnet.carconfigurator.idling.ViewPagerDataReceivedIdlingResource
import com.kacera.thinxnet.carconfigurator.idling.ViewPagerScrollingIdlingResource
import com.kacera.thinxnet.carconfigurator.idling.withIdlingResource
import com.kacera.thinxnet.carconfigurator.matcher.containsDrawable
import com.kacera.thinxnet.carconfigurator.matcher.inActiveViewPagerView
import com.kacera.thinxnet.carconfigurator.matcher.withDrawable
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Matchers.*

abstract class BaseRobot(private val activity: Activity) {

    fun clickOnView(@IdRes viewId: Int) {
        onView(withId(viewId)).perform(click())
    }

    fun clickOnRecyclerViewItemAtPosition(@IdRes recyclerViewId: Int, position: Int) {
        onView(withId(recyclerViewId)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(
                position,
                click()
            )
        )
    }

    fun scrollRecyclerViewItemOnPosition(@IdRes recyclerViewId: Int, position: Int) {
        withIdlingResource(
            RecyclerViewDataReceivedIdlingResource(
                activity,
                R.id.maker_recycler_view
            )
        ) {
            onView(withId(recyclerViewId)).perform(
                scrollToPosition<RecyclerView.ViewHolder>(
                    position
                )
            )
        }
    }

    fun isViewDisplayed(@IdRes viewId: Int) {
        onView(withId(viewId)).check(matches(isDisplayed()))
    }

    fun isTextViewDisplayed(text: String) {
        onView(withText(text)).check(matches(isDisplayed()))
    }

    fun viewContainsText(@IdRes viewId: Int, @StringRes text: Int) {
        viewContainsText(viewId, activity.getString(text))
    }

    fun viewContainsText(@IdRes viewId: Int, text: String) {
        onView(withId(viewId)).check(matches(withText(text)))
    }

    fun viewNotContainsText(@IdRes viewId: Int, @StringRes text: Int) {
        viewNotContainsText(viewId, activity.getString(text))
    }

    fun viewNotContainsText(@IdRes viewId: Int, text: String) {
        onView(withId(viewId)).check(matches(not(withText(text))))
    }

    fun imageViewContainsDrawable(@IdRes viewId: Int) {
        onView(withId(viewId)).check(matches(containsDrawable()))
    }

    fun imageViewContainsDrawable(
        @IdRes viewId: Int,
        @DrawableRes drawableId: Int,
        @ColorInt tintColor: Int? = null
    ) {
        onView(withId(viewId)).check(matches(withDrawable(drawableId, tintColor)))
    }

    fun isViewPagerLoaded(@IdRes viewId: Int) {
        withIdlingResource(ViewPagerDataReceivedIdlingResource(activity, viewId)) {
            onView(
                allOf(
                    instanceOf(Button::class.java),
                    inActiveViewPagerView(withId(R.id.model_pager))
                )
            ).check(matches(isDisplayed()))
        }
    }

    fun isViewPagerLoadedWithText(@IdRes viewId: Int, text: String) {
        withIdlingResource(ViewPagerDataReceivedIdlingResource(activity, viewId)) {
            isTextViewDisplayed(text)
        }
    }

    fun isViewPagerLoadedContainingText(@IdRes viewId: Int, text: String) {
        withIdlingResource(ViewPagerDataReceivedIdlingResource(activity, viewId)) {
            onView(
                allOf(
                    withText(containsString(text)),
                    inActiveViewPagerView(withId(viewId))
                )
            ).check(matches(isDisplayed()))
        }
    }

    fun swipeLeftOnView(@IdRes viewId: Int) {
        withIdlingResource(ViewPagerScrollingIdlingResource(activity, viewId)) {
            onView(withId(viewId)).perform(ViewActions.swipeLeft())
            isViewDisplayed(viewId)
        }
    }
}