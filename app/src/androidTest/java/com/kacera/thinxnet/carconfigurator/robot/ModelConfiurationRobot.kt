package com.kacera.thinxnet.carconfigurator.robot

import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withSpinnerText
import com.kacera.thinxnet.carconfigurator.MainActivity
import com.kacera.thinxnet.carconfigurator.R
import com.kacera.thinxnet.carconfigurator.matcher.inActiveViewPagerView
import com.kacera.thinxnet.carconfigurator.network.data.FuelType
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Matchers.containsString
import org.hamcrest.Matchers.instanceOf
import org.hamcrest.core.Is.`is`

fun modelConfiguration(activity: MainActivity, func: ModelConfigurationRobot.() -> Unit) =
    ModelConfigurationRobot(activity).apply(func)

class ModelConfigurationRobot(private val activity: MainActivity) : BaseRobot(activity) {

    fun isViewPagerLoaded() {
        isViewPagerLoaded(R.id.model_pager)
    }

    fun isRecyclerViewLoadedOnPosition(position: Int) {
        scrollRecyclerViewItemOnPosition(R.id.maker_recycler_view, position)
    }

    fun selectFuelConfiguration(fuelText: String) {
        onView(
            allOf(
                withId(R.id.fuel_type),
                inActiveViewPagerView(withId(R.id.model_pager))
            )
        ).perform(click())
        onData(
            allOf(
                instanceOf(FuelType::class.java),
                `is`(FuelType(3, "Diesel"))
            )
        ).perform(click())
        onView(
            allOf(
                withId(R.id.fuel_type),
                inActiveViewPagerView(withId(R.id.model_pager))
            )
        ).check(matches(withSpinnerText(containsString(fuelText))))
    }

    fun onConfirmClicked(func: PreviewConfigurationRobot.() -> Unit) {
        onView(
            allOf(
                withId(R.id.confirm_button),
                inActiveViewPagerView(withId(R.id.model_pager))
            )
        ).perform(click())
        previewConfiguration(activity, func)
    }

    fun clickOnRecyclerViewItem(position: Int, func: ModelConfigurationRobot.() -> Unit) {
        clickOnRecyclerViewItemAtPosition(R.id.maker_recycler_view, position)
        modelConfiguration(activity, func)
    }

    fun isModelPageLoaded(model: String) {
        isViewPagerLoadedWithText(R.id.model_pager, model)
    }

    fun isMakersModelPageLoaded(maker: String) {
        isViewPagerLoadedContainingText(R.id.model_pager, maker)
    }

    fun swipeLeft() {
        swipeLeftOnView(R.id.model_pager)
    }
}