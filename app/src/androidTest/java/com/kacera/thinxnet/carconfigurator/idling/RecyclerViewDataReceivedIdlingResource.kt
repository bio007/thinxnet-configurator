package com.kacera.thinxnet.carconfigurator.idling

import android.app.Activity
import androidx.annotation.IdRes
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewDataReceivedIdlingResource(
    activity: Activity,
    @IdRes private val recyclerViewId: Int
) : BaseIdlingResource(activity) {

    private val adapter: RecyclerView.Adapter<*>?
        get() = activity.findViewById<RecyclerView>(recyclerViewId)?.adapter

    override fun getName(): String = "RecyclerViewDataReceivedIdlingResource"

    override fun isIdle(): Boolean = adapter?.let { it.itemCount > 0 } ?: false
}