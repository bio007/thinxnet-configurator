package com.kacera.thinxnet.carconfigurator

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.kacera.thinxnet.carconfigurator.network.data.ApiResponse
import com.kacera.thinxnet.carconfigurator.network.data.Model
import com.kacera.thinxnet.carconfigurator.persistance.ConfigurationStore
import com.kacera.thinxnet.carconfigurator.ui.configuration.ModelsFragmentViewModel
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfigurationImageRepository
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfiguratorRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.O])
@ExperimentalCoroutinesApi
@RunWith(RobolectricTestRunner::class)
class ModelsFragmentViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    private val testDispatcher = TestCoroutineDispatcher()

    private lateinit var carRepository: CarConfiguratorRepository

    @Mock
    private lateinit var carImageRepository: CarConfigurationImageRepository

    @Mock
    private lateinit var store: ConfigurationStore

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        stopKoin()
    }

    @Test
    fun `ViewModel displays correct loading values`() = testDispatcher.runBlockingTest {
        carRepository = mock {
            onBlocking { getModels(any()) } doReturn (ApiResponse.Success(emptyList()))
        }

        val viewModel = ModelsFragmentViewModel(carRepository, carImageRepository, store, 1, -1)
        pauseDispatcher()
        viewModel.onCreate(mock())
        viewModel.loading.test().assertValue(true)
        viewModel.apiError.test().assertValue(false)
        runCurrent()
        viewModel.loading.test().assertValue(false)
        viewModel.apiError.test().assertValue(false)
    }

    @Test
    fun `ViewModel loads items from remote`() = testDispatcher.runBlockingTest {
        carRepository = mock {
            onBlocking { getModels(any()) } doReturn (ApiResponse.Success(
                listOf(Model(4002, "R7", 1009, "Audi", emptyList()))
            ))
        }

        val viewModel = ModelsFragmentViewModel(carRepository, carImageRepository, store, 1009, -1)
        viewModel.onCreate(mock())
        Assert.assertEquals(1, viewModel.adapter.itemCount)
        Assert.assertEquals(4002, viewModel.adapter.getItemId(0))
    }

    @Test
    fun `ViewModel propagates error from remote`() = testDispatcher.runBlockingTest {
        carRepository = mock {
            onBlocking { getModels(any()) } doReturn (ApiResponse.Error(RuntimeException()))
        }

        val viewModel = ModelsFragmentViewModel(carRepository, carImageRepository, store, 1, -1)
        pauseDispatcher()
        viewModel.onCreate(mock())
        viewModel.loading.test().assertValue(true)
        viewModel.apiError.test().assertValue(false)
        runCurrent()
        viewModel.loading.test().assertValue(false)
        viewModel.apiError.test().assertValue(true)
    }

    @Test
    fun `ViewModel preselects correct item`() = testDispatcher.runBlockingTest {
        carRepository = mock {
            onBlocking { getModels(any()) } doReturn (ApiResponse.Success(
                listOf(
                    Model(4002, "R7", 1009, "Audi", emptyList()),
                    Model(4003, "R7", 1009, "Audi", emptyList()),
                    Model(4004, "R7", 1009, "Audi", emptyList()),
                    Model(4005, "R7", 1009, "Audi", emptyList())
                )
            ))
        }

        val viewModel = ModelsFragmentViewModel(carRepository, carImageRepository, store, 1009, 4004)
        val selectedItemObserver = viewModel.selectedItem.test()
        selectedItemObserver.assertNoValue()
        viewModel.onCreate(mock())
        selectedItemObserver.assertHasValue()
        selectedItemObserver.assertValue(2)
    }
}