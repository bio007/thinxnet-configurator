package com.kacera.thinxnet.carconfigurator

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.kacera.thinxnet.carconfigurator.network.data.ApiResponse
import com.kacera.thinxnet.carconfigurator.network.data.Maker
import com.kacera.thinxnet.carconfigurator.ui.configuration.MakersFragmentViewModel
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfigurationImageRepository
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfiguratorRepository
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.O])
@ExperimentalCoroutinesApi
@RunWith(RobolectricTestRunner::class)
class MakersFragmentViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    private val testDispatcher = TestCoroutineDispatcher()

    private lateinit var carRepository: CarConfiguratorRepository

    @Mock
    private lateinit var carImageRepository: CarConfigurationImageRepository

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        stopKoin()
    }

    @Test
    fun `ViewModel displays correct loading values`() = testDispatcher.runBlockingTest {
        carRepository = mock {
            onBlocking { getMakers() } doReturn (ApiResponse.Success(emptyList()))
        }

        val viewModel = MakersFragmentViewModel(carRepository, carImageRepository)
        pauseDispatcher()
        viewModel.onCreate(mock())
        viewModel.loading.test().assertValue(true)
        viewModel.apiError.test().assertValue(false)
        runCurrent()
        viewModel.loading.test().assertValue(false)
        viewModel.apiError.test().assertValue(false)
    }

    @Test
    fun `ViewModel loads items from remote`() = testDispatcher.runBlockingTest {
        carRepository = mock {
            onBlocking { getMakers() } doReturn (ApiResponse.Success(listOf(Maker(1009, "Audi"))))
        }

        val viewModel = MakersFragmentViewModel(carRepository, carImageRepository)
        viewModel.onCreate(mock())
        Assert.assertEquals(1, viewModel.adapter.itemCount)
        Assert.assertEquals(1009, viewModel.adapter.getItemId(0))
    }

    @Test
    fun `ViewModel propagates error from remote`() = testDispatcher.runBlockingTest {
        carRepository = mock {
            onBlocking { getMakers() } doReturn (ApiResponse.Error(RuntimeException()))
        }

        val viewModel = MakersFragmentViewModel(carRepository, carImageRepository)
        pauseDispatcher()
        viewModel.onCreate(mock())
        viewModel.loading.test().assertValue(true)
        viewModel.apiError.test().assertValue(false)
        runCurrent()
        viewModel.loading.test().assertValue(false)
        viewModel.apiError.test().assertValue(true)
    }
}