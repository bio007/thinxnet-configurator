package com.kacera.thinxnet.carconfigurator

import android.net.Uri
import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.preference.PreferenceManager
import androidx.test.platform.app.InstrumentationRegistry
import com.jraska.livedata.test
import com.kacera.thinxnet.carconfigurator.persistance.SharedPreferencesConfigurationStore
import com.kacera.thinxnet.carconfigurator.persistance.data.CarConfiguration
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.O])
@RunWith(RobolectricTestRunner::class)
class SharedPreferencesConfigurationStoreTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    private val appContext = InstrumentationRegistry.getInstrumentation().targetContext

    private lateinit var store: SharedPreferencesConfigurationStore

    @Before
    fun setUp() {
        store = SharedPreferencesConfigurationStore(appContext)
    }

    @After
    fun cleanUp() {
        stopKoin()
        PreferenceManager.getDefaultSharedPreferences(appContext).edit().apply() {
            clear()
            apply()
        }
    }

    @Test
    fun `Get empty configuration`() {
        store.getConfiguration().test().assertHasValue().assertValue(CarConfiguration.EMPTY)
    }

    @Test
    fun `Save car configuration to shared preferences`() {
        store.getConfiguration().test().assertHasValue().assertValue(CarConfiguration.EMPTY)

        val testConfiguration = CarConfiguration(
            "Ferrari",
            21,
            "F150",
            32,
            "Benzin",
            1,
            Uri.parse("https://example.api/ferrari.jpg")
        )

        store.saveConfiguration(testConfiguration)
        store.getConfiguration().test().assertHasValue().assertValue(testConfiguration)
    }
}