package com.kacera.thinxnet.carconfigurator

import com.kacera.thinxnet.carconfigurator.network.ConfiguratorApi
import com.kacera.thinxnet.carconfigurator.network.data.*
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfiguratorRepository
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfiguratorRepositoryImpl
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.doThrow
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class CarConfiguratorRepositoryTest {

    private val testDispatcher = TestCoroutineDispatcher()

    private lateinit var api: ConfiguratorApi

    private val repository: CarConfiguratorRepository by lazy { CarConfiguratorRepositoryImpl(api) }

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `Get makers from the remote`() = testDispatcher.runBlockingTest {
        val expectedMakers = listOf(
            Maker(1, "Fiat"),
            Maker(2, "Skoda"),
            Maker(3, "Audi")
        )

        val makers = Makers(expectedMakers)

        api = mock {
            onBlocking { getMakers() } doReturn (makers)
        }

        val response = repository.getMakers()
        Assert.assertThat(response, instanceOf(ApiResponse.Success::class.java))
        Assert.assertEquals(expectedMakers, (response as ApiResponse.Success).result)
    }

    @Test
    fun `Get models from the remote`() = testDispatcher.runBlockingTest {
        val expectedModels = listOf(
            Model(1, "Punto", 1, "Fiat", listOf(FuelType(1, "diesel"))),
            Model(2, "Octavia", 2, "Skoda", emptyList()),
            Model(3, "RS7", 3, "Audi", listOf(FuelType(1, "benzine")))
        )

        val models = Models(expectedModels)

        api = mock {
            onBlocking { getModels(any()) } doReturn (models)
        }

        val response = repository.getModels(any())
        Assert.assertThat(response, instanceOf(ApiResponse.Success::class.java))
        Assert.assertEquals(expectedModels, (response as ApiResponse.Success).result)
    }

    @Test
    fun `Return error from the remote`() = testDispatcher.runBlockingTest {
        api = mock {
            onBlocking { getMakers() } doThrow (RuntimeException())
            onBlocking { getModels(any()) } doThrow (RuntimeException())
        }

        val makersResponse = repository.getMakers()
        Assert.assertThat(makersResponse, instanceOf(ApiResponse.Error::class.java))
        Assert.assertTrue((makersResponse as ApiResponse.Error).error is RuntimeException)

        val modelsResponse = repository.getModels(any())
        Assert.assertThat(modelsResponse, instanceOf(ApiResponse.Error::class.java))
        Assert.assertTrue((modelsResponse as ApiResponse.Error).error is RuntimeException)
    }
}