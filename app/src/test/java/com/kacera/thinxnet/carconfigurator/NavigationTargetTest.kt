package com.kacera.thinxnet.carconfigurator

import androidx.fragment.app.*
import com.kacera.thinxnet.carconfigurator.ui.navigation.NavigationTarget
import com.nhaarman.mockitokotlin2.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NavigationTargetTest {

    @Mock
    private lateinit var fragmentManager: FragmentManager

    private lateinit var transaction: FragmentTransaction

    @Mock
    private lateinit var mockedFragment: Fragment

    @Before
    fun setUp() {
        transaction = mock<TestFragmentTransaction>(useConstructor = UseConstructor.withArguments(
            mock<FragmentFactory>(). apply {
                whenever(this.instantiate(any(), any())).thenReturn(mockedFragment)
            },
            mock<ClassLoader>())
        )
        whenever(transaction.replace(any(), any<Fragment>(), anyOrNull())).thenReturn(transaction)
        whenever(fragmentManager.beginTransaction()).thenReturn(transaction)
    }

    @Test
    fun `Navigate to destination`() {
        NavigationTarget.EmptyConfigurationFragmentTarget.navigate(fragmentManager)
        verify(transaction).replace(any(), eq(mockedFragment), anyOrNull())
        verify(transaction).commit()
    }

    @Test
    fun `Navigate, don't add to back stack`() {
        NavigationTarget.EmptyConfigurationFragmentTarget.navigate(fragmentManager)
        verify(transaction, never()).addToBackStack(anyOrNull())
    }

    @Test
    fun `Navigate, add to back stack`() {
        NavigationTarget.EmptyConfigurationFragmentTarget.addToBackStack("tag").navigate(fragmentManager)
        verify(transaction).addToBackStack(eq("tag"))
    }

    @Test
    fun `Navigate, clear back stack`() {
        whenever(fragmentManager.backStackEntryCount).thenReturn(1)
        whenever(fragmentManager.getBackStackEntryAt(any())).thenReturn(mock())

        NavigationTarget.EmptyConfigurationFragmentTarget.clearBackStack().navigate(fragmentManager)
        verify(transaction, never()).addToBackStack(anyOrNull())
        verify(fragmentManager).popBackStack(any<Int>(), any())
    }
}