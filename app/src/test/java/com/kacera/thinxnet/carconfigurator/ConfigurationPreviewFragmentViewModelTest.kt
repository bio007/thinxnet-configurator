package com.kacera.thinxnet.carconfigurator

import android.net.Uri
import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.kacera.thinxnet.carconfigurator.persistance.data.CarConfiguration
import com.kacera.thinxnet.carconfigurator.ui.configuration.data.CarConfigurationImageRepository
import com.kacera.thinxnet.carconfigurator.ui.preview.ConfigurationPreviewFragmentViewModel
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.O])
@ExperimentalCoroutinesApi
@RunWith(RobolectricTestRunner::class)
class ConfigurationPreviewFragmentViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    private lateinit var carImageRepository: CarConfigurationImageRepository

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        stopKoin()
    }

    @Test
    fun `ViewModel call image api if stored url is null`() = testDispatcher.runBlockingTest {
        val expectedConfiguration = CarConfiguration("Audi", 1, "RS7", 1, "diesel", 2, null)

        carImageRepository = mock {
            onBlocking { getImageRequest(anyOrNull()) } doReturn mock()
        }

        ConfigurationPreviewFragmentViewModel(expectedConfiguration, carImageRepository)
        verify(carImageRepository).getCarModelImage(
            eq("${expectedConfiguration.maker} ${expectedConfiguration.model}"),
            any()
        )
    }

    @Test
    fun `ViewModel doesn't call image api if stored url is not null`() =
        testDispatcher.runBlockingTest {
            val expectedConfiguration = CarConfiguration(
                "Audi",
                1,
                "RS7",
                1,
                "diesel",
                2,
                Uri.parse("https://api.example.com/test.jpg")
            )

            carImageRepository = mock {
                onBlocking { getImageRequest(anyOrNull()) } doReturn mock()
            }

            ConfigurationPreviewFragmentViewModel(expectedConfiguration, carImageRepository)
            verify(carImageRepository, never()).getCarModelImage(any(), any())
        }
}