package androidx.fragment.app


abstract class TestFragmentTransaction(
    fragmentFactory: FragmentFactory,
    classloader: ClassLoader?
) :
    FragmentTransaction(fragmentFactory, classloader)