# Car Configurator

Simple car configurator assignment app for ThinxNet company.

The app is using [Unsplash](https://unsplash.com/) API to download **random** images of cars and car makers based on names of the models and makers. This API is limited for the number of requests so if you see placeholders instead this is the reason. Number of requests per hour is only **50** for now.

![Car Configurator](screenshot.png "Car Configurator")

UI is bit raw and simple as design hasn't been provided and main focus was not on the looks 😶.